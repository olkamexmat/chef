package by.shevtsova.chef.creator;

import by.shevtsova.chef.exception.TechnicalException;
import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.reader.ReadFromFile;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.ArrayList;

/**
 * Fill list with vegetables from file
 * 
 * @author Volha_Shautsova
 *
 */
public class Creator {
	private static final Logger LOGGER = Logger.getLogger(Creator.class);

	public static Salad create(String fileName) {
		List<Vegetable> vegetableList = new ArrayList<Vegetable>();
		try {
			vegetableList = ReadFromFile.read(fileName, vegetableList);
		} catch (TechnicalException e) {
			LOGGER.error("Fail in creating list");
		}
		return new Salad(vegetableList);
	}
}
