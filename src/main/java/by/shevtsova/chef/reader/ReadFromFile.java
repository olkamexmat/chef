package by.shevtsova.chef.reader;

import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;
import by.shevtsova.chef.model.vegetable.LeguminousVegetable;
import by.shevtsova.chef.model.vegetable.RootVegetable;
import by.shevtsova.chef.exception.TechnicalException;
import by.shevtsova.chef.model.vegetable.LeafVegetable;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Scanner;

/**
 * Reading values from files
 * 
 * @author Volha_Shautsova
 *
 */
public class ReadFromFile {
	private static final Logger LOGGER = Logger.getLogger(ReadFromFile.class);

	private enum TypeOfVegetable {
		FRUITING, LEAF, LEGUMINOUS, ROOT
	}

	/**
	 * Read vegetables from file info/vegetables.txt
	 * 
	 * @param fileName
	 *            fileName
	 * @param vegetables
	 *            {@code List<Vegetable>}
	 * @return the vegetables list
	 * @throws TechnicalException
	 *             exception
	 */
	public static List<Vegetable> read(String fileName, List<Vegetable> vegetables) throws TechnicalException {
		int caloricContent, weight;
		String name;
		TypeOfVegetable type;
		Scanner scanner = null;
		try {
			scanner = new Scanner(new FileReader(fileName));
			while (scanner.hasNext()) {
				Vegetable vegetable;
				type = TypeOfVegetable.valueOf(scanner.next().toUpperCase());
				caloricContent = scanner.nextInt();
				weight = scanner.nextInt();
				name = scanner.next();
				switch (type) {
				case FRUITING: {
					vegetable = new FruitingVegetable(caloricContent, weight, name);
					vegetables.add(vegetable);
					break;
				}
				case LEAF: {
					vegetable = new LeafVegetable(caloricContent, weight, name);
					vegetables.add(vegetable);
					break;
				}
				case LEGUMINOUS: {
					vegetable = new LeguminousVegetable(caloricContent, weight, name);
					vegetables.add(vegetable);
					break;
				}
				case ROOT: {
					vegetable = new RootVegetable(caloricContent, weight, name);
					vegetables.add(vegetable);
					break;
				}
				default:
					throw new TechnicalException("Wrong type of vegetable");
				}
			}
		} catch (FileNotFoundException exc) {
			LOGGER.error(exc);
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		return vegetables;
	}

	/**
	 * Read amount of minimum and maximum values for caloricContent
	 * 
	 * @param fileName
	 *            fileName
	 * @return the int value
	 */
	public static int getAmount(String fileName) {
		int amount = 0;
		Scanner scanner = null;
		try {
			scanner = new Scanner(new FileReader(fileName));
			while (scanner.hasNext()) {
				if (scanner.hasNextInt()) {
					amount = scanner.nextInt();
				}
			}
		} catch (FileNotFoundException exc) {
			LOGGER.error(exc);
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		return amount;
	}
}
