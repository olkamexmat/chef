package by.shevtsova.chef.output;

import by.shevtsova.chef.action.CalculateCaloricContent;
import by.shevtsova.chef.action.SampleVegetable;
import by.shevtsova.chef.action.sort.Sort;
import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.exception.LogicException;
import org.apache.log4j.Logger;

/**
 * Class performing logging info to file log.txt located in the folder log/
 * 
 * @author Volha_Shautsova
 *
 */
public class Output {
	private static final Logger LOGGER = Logger.getLogger(Output.class);

	public static void printToLogSalad(Salad salad) {
		LOGGER.info(salad);
	}

	public static void printToLogCaloricContentOfSalad(Salad salad) {
		try {
			LOGGER.info("Amount of calories in salad is " + CalculateCaloricContent.calculate(salad));
		} catch (LogicException e) {
			LOGGER.error("Couldn't calculate caloric content");
		}
	}

	public static void printToLogSortedVegetable(Salad salad) {
		try {
			LOGGER.info("Sorted vegetables in salad : " + Sort.sortByWeight(salad));
		} catch (CloneNotSupportedException e) {
			LOGGER.error(e);
		}
	}

	public static void printToLogSampledVegetable(Salad salad) {
		LOGGER.info("Suitable vegetables: " + SampleVegetable.sampleVegetable(salad));
	}
}
