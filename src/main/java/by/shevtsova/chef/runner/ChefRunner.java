package by.shevtsova.chef.runner;

import by.shevtsova.chef.creator.Creator;
import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.output.Output;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Main class performing some actions
 * 
 * @author Volha_Shautsova
 *
 */
public class ChefRunner {
	private static Logger LOGGER = Logger.getLogger(ChefRunner.class);
	private static final String LOG_FILE_NAME = "log\\log4j.xml";
	private static final String FILE_NAME = "info/vegetables";

	static {
		new DOMConfigurator().doConfigure(LOG_FILE_NAME, LogManager.getLoggerRepository());
	}

	public static void main(String[] args) {
		LOGGER.info("Application starts");

		Salad salad = Creator.create(FILE_NAME);

		Output.printToLogSalad(salad);
		Output.printToLogCaloricContentOfSalad(salad);
		Output.printToLogSortedVegetable(salad);
		Output.printToLogSampledVegetable(salad);

		LOGGER.info("Application ends");
	}
}
