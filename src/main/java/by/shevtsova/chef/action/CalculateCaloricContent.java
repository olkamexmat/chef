package by.shevtsova.chef.action;

import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.exception.LogicException;

/**
 * Class for calculating calories number for salad {@link Salad}
 * 
 * @author Volha_Shautsova
 *
 */
public class CalculateCaloricContent {

	public static int calculate(Salad salad) throws LogicException {
		int commonCaloricContent = 0;
		
		if (salad != null) {
			for (Vegetable vegetable : salad.getVegetableList()) {
				commonCaloricContent += vegetable.getCaloricContent();
			}
		} else {
			throw new LogicException("Salad doesn't exist. Impossible calculate amount of calories");
		}
		
		return commonCaloricContent;
	}
}
