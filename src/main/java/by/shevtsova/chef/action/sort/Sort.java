package by.shevtsova.chef.action.sort;

import by.shevtsova.chef.model.Salad;

import java.util.Collections;

/**
 * Sorting vegetables in salad by weight
 * 
 * @author Volha_Shautsova
 *
 */
public class Sort {

	public static Salad sortByWeight(Salad salad) throws CloneNotSupportedException {
		Salad tmp = (Salad) salad.clone();
		Collections.sort(tmp.getVegetableList(), new SortingByWeight());
		return salad;
	}
}
