package by.shevtsova.chef.action.sort;

import by.shevtsova.chef.model.Vegetable;

import java.util.Comparator;

/**
 * Compare two vegetables by their weights
 * 
 * @author Volha_Shautsova
 *
 */
public class SortingByWeight implements Comparator<Vegetable> {
	@Override
	public int compare(Vegetable vegetable1, Vegetable vegetable2) {
		int returnValue;
		if (vegetable1.getWeight() < vegetable2.getWeight()) {
			returnValue = -1;
		} else {
			if (vegetable1.getWeight() > vegetable2.getWeight()) {
				returnValue = 1;
			} else {
				returnValue = 0;
			}
		}
		return returnValue;
	}
}
