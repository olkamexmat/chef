package by.shevtsova.chef.action;

import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.reader.ReadFromFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Get a sample of vegetables from salad
 * 
 * @author Volha_Shautsova
 *
 */
public class SampleVegetable {
	private static final String FILE_NAME_MIN = "info/minCalorie";
	private static final String FILE_NAME_MAX = "info/maxCalorie";

	public static List<Vegetable> sampleVegetable(Salad salad) {
		List<Vegetable> vegetables = new ArrayList<Vegetable>();
		int minAmount = ReadFromFile.getAmount(FILE_NAME_MIN);
		int maxAmount = ReadFromFile.getAmount(FILE_NAME_MAX);

		for (Vegetable vegetable : salad.getVegetableList()) {
			if (vegetable.getCaloricContent() < maxAmount && vegetable.getCaloricContent() > minAmount) {
				vegetables.add(vegetable);
			}
		}

		return vegetables;
	}
}
