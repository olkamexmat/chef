package by.shevtsova.chef.model.vegetable;

import by.shevtsova.chef.exception.LogicException;
import by.shevtsova.chef.model.Vegetable;
import org.apache.log4j.Logger;

/**
 * Vegetable with type Leguminous
 * Has name that distinct from another vegetables types
 * 
 * @author Volha_Shautsova
 *
 */
public class LeguminousVegetable extends Vegetable {
	private static final Logger LOGGER = Logger.getLogger(LeguminousVegetable.class);

	private String name;

	public LeguminousVegetable(int caloricContent, int weight, String name) {
		super(caloricContent, weight);
		try {
			setName(name);
		} catch (LogicException e) {
			LOGGER.error(e);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws LogicException {
		if (name != null) {
			this.name = name;
		} else {
			throw new LogicException("Name must be defined!");
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		LeguminousVegetable that = (LeguminousVegetable) o;

		if (name != null ? !name.equals(that.name) : that.name != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		String str = super.toString();
		return "LeguminousVegetable: " + str + ", " + "name = " + name + '\n';
	}
}
