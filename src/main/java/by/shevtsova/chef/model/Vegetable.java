package by.shevtsova.chef.model;

import by.shevtsova.chef.exception.LogicException;
import org.apache.log4j.Logger;

/**
 * The common description for vegetable that have calorie amount and weight
 * 
 * @author Volha_Shautsova
 *
 */
public abstract class Vegetable {
	private static Logger LOGGER = Logger.getLogger(Vegetable.class);
	private static final int MIN_AMOUNT_CALORIES = 0;
	private static final int MIN_WEIGHT = 0;

	private int caloricContent;
	private int weight;

	public Vegetable(int caloricContent, int weight) {
		try {
			setCaloricContent(caloricContent);
			setWeight(weight);
		} catch (LogicException e) {
			LOGGER.error(e);
		}
	}

	public int getCaloricContent() {
		return caloricContent;
	}

	public void setCaloricContent(int caloricContent) throws LogicException {
		if (caloricContent >= MIN_AMOUNT_CALORIES) {
			this.caloricContent = caloricContent;
		} else {
			throw new LogicException("Amount of calories can't be negative!");
		}
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(int weight) throws LogicException {
		if (weight > MIN_WEIGHT) {
			this.weight = weight;
		} else {
			throw new LogicException("The weight of vegetable can't be negative!");
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;

		Vegetable vegetable = (Vegetable) obj;

		if (caloricContent != vegetable.caloricContent)
			return false;
		if (weight != vegetable.weight)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = caloricContent;
		result = 31 * result + weight;
		return result;
	}

	@Override
	public String toString() {
		return "caloricContent = " + caloricContent + ", weight = " + weight;
	}
}
