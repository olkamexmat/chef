package by.shevtsova.chef.model;

import by.shevtsova.chef.exception.LogicException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Salad is the mix of vegetables {@link Vegetable} and its inheritors
 * 
 * @author Volha_Shautsova
 *
 */
public class Salad implements Cloneable {
	private static final Logger LOGGER = Logger.getLogger(Salad.class);

	private List<Vegetable> vegetableList;

	public Salad(List<Vegetable> vegetableList) {
		this.vegetableList = new ArrayList<Vegetable>();
		try {
			for (Vegetable vegetable : vegetableList) {
				setVegetableList(vegetable);
			}
		} catch (LogicException e) {
			LOGGER.error(e);
		}
	}

	public List<Vegetable> getVegetableList() {
		return vegetableList;
	}

	public void setVegetableList(Vegetable vegetable) throws LogicException {
		if (vegetable != null) {
			add(vegetable);
		} else {
			throw new LogicException("Salad must have vegetables!");
		}
	}

	public boolean add(Vegetable vegetable) {
		return vegetableList.add(vegetable);
	}

	public Vegetable get(int index) {
		return vegetableList.get(index);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vegetableList == null) ? 0 : vegetableList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Salad other = (Salad) obj;
		if (vegetableList == null) {
			if (other.vegetableList != null)
				return false;
		} else if (!vegetableList.equals(other.vegetableList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Salad:" + '\n' + "vegetableList: " + vegetableList;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
