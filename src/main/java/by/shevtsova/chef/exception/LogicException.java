package by.shevtsova.chef.exception;

/**
 * Exception for logic problems
 * 
 * @author Volha_Shautsova
 *
 */
public class LogicException extends Exception {
	private static final long serialVersionUID = 2280534412953241801L;

	public LogicException(String message) {
		super(message);
	}

}
