package by.shevtsova.chef.exception;

/**
 *  
 * @author Volha_Shautsova
 *
 */
public class TechnicalException extends Exception {
	private static final long serialVersionUID = -1447727765276186303L;

	public TechnicalException(String message) {
		super(message);
	}
}
