package by.shevtsova.chef.reader;

import java.util.ArrayList;

import by.shevtsova.chef.exception.TechnicalException;
import by.shevtsova.chef.model.Vegetable;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ReadFromFileTest {
	private static final String FILE_NAME_MIN = "info/minCalorie";
	private static final String FILE_NAME_MAX = "info/maxCalorie";
	private static final String FILE_NAME_VEGS = "info/vegetables";	
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
		
	/**
	 * Test without any exception
	 * 
	 * @throws TechnicalException
	 */
	@Test
	public void testRead() throws TechnicalException {
		Assert.assertNotNull(ReadFromFile.read(FILE_NAME_VEGS, new ArrayList<Vegetable>()));
	}

	/**
	 * Test wrong fileName
	 * 
	 * @throws TechnicalException
	 */
	@Test
	public void testReadWithWrongFileName() throws TechnicalException {
		Assert.assertNotNull(ReadFromFile.read("file", new ArrayList<Vegetable>()));
	}

	/**
	 * Test without exception
	 */
	@Test
	public void testGetAmount() {
		Assert.assertEquals(47, ReadFromFile.getAmount(FILE_NAME_MAX));
		Assert.assertEquals(13, ReadFromFile.getAmount(FILE_NAME_MIN));
	}
	
	/**
	 * Test wrong fileName
	 */
	@Test
	public void testGetAmountWrongFileName() {		
		Assert.assertEquals(0, ReadFromFile.getAmount("file"));
	}
		
}
