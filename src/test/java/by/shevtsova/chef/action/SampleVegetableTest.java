package by.shevtsova.chef.action;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;

@RunWith(MockitoJUnitRunner.class)
public class SampleVegetableTest {
	@Mock
	private Salad salad;

	/**
	 * When all is OK
	 */
	@Test
	public void testSampleVegetable() {
		Assert.assertNotNull(SampleVegetable.sampleVegetable(salad));
	}

	/**
	 * Min amount of calories less than in minCalorie file
	 */
	@Test
	public void testSampleVegetableNotSutisfyMinAmount() {
		List<Vegetable> list = new ArrayList<Vegetable>();
		Vegetable vegetable = new FruitingVegetable(10, 0, "fruit");
		list.add(vegetable);

		Mockito.when(salad.getVegetableList()).thenReturn(list);

		Assert.assertNotNull(SampleVegetable.sampleVegetable(salad));
	}

	/**
	 * Max amount of calories more than in maxCalorie file
	 */
	@Test
	public void testSampleVegetableNotSutisfyMaxAmount() {
		List<Vegetable> list = new ArrayList<Vegetable>();
		Vegetable vegetable = new FruitingVegetable(50, 0, "fruit");
		list.add(vegetable);

		Mockito.when(salad.getVegetableList()).thenReturn(list);

		Assert.assertNotNull(SampleVegetable.sampleVegetable(salad));
	}

	/**
	 * Min amount of calories more than in minCalorie file 
	 * and
	 * Max amount of calories less than in maxCalorie file
	 */
	@Test
	public void testSampleVegetableSutisfyBothCond() {
		List<Vegetable> list = new ArrayList<Vegetable>();
		Vegetable vegetable = new FruitingVegetable(15, 0, "fruit");
		list.add(vegetable);

		Mockito.when(salad.getVegetableList()).thenReturn(list);

		Assert.assertNotNull(SampleVegetable.sampleVegetable(salad));
	}
}
