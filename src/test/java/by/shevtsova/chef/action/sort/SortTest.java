package by.shevtsova.chef.action.sort;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;

@RunWith(MockitoJUnitRunner.class)
public class SortTest {
	@Mock
	private Salad salad;

	/**
	 * The first vegetables after sorting should be different
	 * 
	 * @throws CloneNotSupportedException
	 */
	@Test
	public void testSortByWeight() throws CloneNotSupportedException {
		Mockito.when(salad.clone()).thenReturn(salad);
		
		List<Vegetable> list = new ArrayList<Vegetable>();
		Vegetable vegetable1 = new FruitingVegetable(20, 12, "fruit");
		Vegetable vegetable2 = new FruitingVegetable(20, 5, "fruit");
		list.add(vegetable1);
		list.add(vegetable2);

		Mockito.when(salad.getVegetableList()).thenReturn(list);
		
		Assert.assertNotSame(salad.getVegetableList().get(0).getWeight(),
				Sort.sortByWeight(salad).getVegetableList().get(0).getWeight()); 
	}

}
