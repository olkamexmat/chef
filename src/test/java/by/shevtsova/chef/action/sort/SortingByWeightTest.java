package by.shevtsova.chef.action.sort;

import by.shevtsova.chef.exception.LogicException;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SortingByWeightTest {

	@Mock
	private FruitingVegetable mockVegetable;

	/**
	 * Expected 0 because weights are equaled
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testCompareEquals() throws LogicException {
		mockVegetable.setWeight(1);
		SortingByWeight sorting = new SortingByWeight();

		Assert.assertEquals(0, sorting.compare(mockVegetable, mockVegetable));
	}

	/**
	 * Expected 1 because first weight more than second
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testCompareMore() throws LogicException {
		Vegetable vegetable = new FruitingVegetable(2, 2, "fruit");
		SortingByWeight sorting = new SortingByWeight();

		Assert.assertEquals(1, sorting.compare(vegetable, mockVegetable));
	}
	
	/**
	 * Expected -1 because first weight less than second
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testCompareLess() throws LogicException {
		Vegetable vegetable = new FruitingVegetable(2, 2, "fruit");
		SortingByWeight sorting = new SortingByWeight();

		Assert.assertEquals(-1, sorting.compare(mockVegetable, vegetable));
	}

}
