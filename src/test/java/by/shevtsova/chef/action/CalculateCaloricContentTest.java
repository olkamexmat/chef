package by.shevtsova.chef.action;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import by.shevtsova.chef.exception.LogicException;
import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;

@RunWith(MockitoJUnitRunner.class)
public class CalculateCaloricContentTest {
	private final static int CALORIC_CONTENT = 10;
	
	@Mock
	private Salad salad;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();

	/**
	 * Test when salad doesn't exist
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testCalculateWithExc() throws LogicException {
		thrown.expect(LogicException.class);
		
		CalculateCaloricContent.calculate(null);
	}

	/**
	 * All is OK
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testCalculate() throws LogicException {
		List<Vegetable> list = new ArrayList<Vegetable>();
		Vegetable vegetable = new FruitingVegetable(CALORIC_CONTENT, 0, "fruit");
		list.add(vegetable);

		Mockito.when(salad.getVegetableList()).thenReturn(list);
		Assert.assertEquals(CALORIC_CONTENT, CalculateCaloricContent.calculate(salad));
	}

	/**
	 * Expected exc because of vegetable list in null
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testCalculateNull() throws LogicException {
		thrown.expect(NullPointerException.class);
		
		Mockito.when(salad.getVegetableList()).thenReturn(null);
		CalculateCaloricContent.calculate(salad);
	}
}
