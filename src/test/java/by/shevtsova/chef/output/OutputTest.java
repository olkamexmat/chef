package by.shevtsova.chef.output;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;
import by.shevtsova.chef.exception.LogicException;

/**
 * Test for checking methods work in {@link Output}
 * 
 * @author Volha_Shautsova
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class OutputTest {
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Mock
	private Salad salad;

	@Spy
	private Output outputSpy;

	@Test
	public void testPrintToLogSalad() {
		Output.printToLogSalad(salad);
	}

	@Test
	public void testPrintToLogCaloricContentOfSalad() throws LogicException {
		Output.printToLogCaloricContentOfSalad(salad);
	}

	@Test
	public void testPrintToLogSortedVegetable() {
		Output.printToLogSampledVegetable(salad);
	}

	@Test
	public void testPrintToLogSampledVegetable() {
		List<Vegetable> list = new ArrayList<Vegetable>();
		Vegetable vegetable1 = new FruitingVegetable(20, 12, "fruit");
		Vegetable vegetable2 = new FruitingVegetable(20, 5, "fruit");
		list.add(vegetable1);
		list.add(vegetable2);

		salad = new Salad(list);

		Output.printToLogSortedVegetable(salad);
	}

}
