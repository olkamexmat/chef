package by.shevtsova.chef.model;

import org.junit.Assert;
import org.junit.Test;

import by.shevtsova.chef.exception.LogicException;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;
import by.shevtsova.chef.model.vegetable.RootVegetable;
import by.shevtsova.chef.model.vegetable.LeafVegetable;
import by.shevtsova.chef.model.vegetable.LeguminousVegetable;

public class VegetableTest {
	private final static int CALORIES = 15;
	private final static int WEIGHT = 10;
	private final static String NAME_FIRST = "NAME_1";
	private final static String NAME_SECOND = "NAME_2";
	
	private Vegetable vegetableFirst;
	private Vegetable vegetableSecond;
	private Vegetable vegetableThird;
	private Vegetable vegetableFourth;

	/**
	 * Check equals method for {@link FruitingVegetable}
	 */
	@Test
	public void testEqualsFruiting() {
		vegetableFirst = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetableSecond = new FruitingVegetable(CALORIES, WEIGHT, NAME_SECOND);

		Assert.assertFalse(vegetableFirst.equals(vegetableSecond));
		vegetableSecond = vegetableFirst;
		Assert.assertTrue(vegetableFirst.equals(vegetableSecond));
		Assert.assertFalse(vegetableFirst.equals(null));
		
		vegetableThird = new FruitingVegetable(-CALORIES, -WEIGHT, NAME_SECOND);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new FruitingVegetable(-CALORIES, -WEIGHT, null);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		Assert.assertTrue(vegetableFirst.equals(vegetableThird)); 
	}

	/**
	 * Name cann't be null
	 * 
	 * @throws LogicException
	 */
	@Test(expected = LogicException.class)
	public void testNameFruiting() throws LogicException {
		FruitingVegetable vegetable = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetable.setName(null);
	}

	/**
	 * Calories cann't be less than 0
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testCalories() throws LogicException {
		vegetableFirst = new FruitingVegetable(-CALORIES, WEIGHT, NAME_FIRST);
		Assert.assertEquals(0, vegetableFirst.getCaloricContent());
	}

	/**
	 * Check equals method for {@link RootVegetable}
	 */
	@Test
	public void testEqualsRootVegetable() {
		vegetableFirst = new RootVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetableSecond = new RootVegetable(CALORIES, WEIGHT, NAME_SECOND);

		Assert.assertFalse(vegetableFirst.equals(vegetableSecond));
		vegetableSecond = vegetableFirst;
		Assert.assertTrue(vegetableFirst.equals(vegetableSecond));
		Assert.assertFalse(vegetableFirst.equals(null));
		
		vegetableThird = new RootVegetable(-CALORIES, -WEIGHT, NAME_SECOND);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new RootVegetable(-CALORIES, -WEIGHT, null);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new RootVegetable(CALORIES, WEIGHT, NAME_FIRST);
		Assert.assertTrue(vegetableFirst.equals(vegetableThird)); 
	}

	/**
	 * Check equals method for {@link LeafVegetable}
	 */
	@Test
	public void testEqualsLeafVegetable() {
		vegetableFirst = new LeafVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetableSecond = new LeafVegetable(CALORIES, WEIGHT, NAME_SECOND);

		Assert.assertFalse(vegetableFirst.equals(vegetableSecond));
		vegetableSecond = vegetableFirst;
		Assert.assertTrue(vegetableFirst.equals(vegetableSecond));
		Assert.assertFalse(vegetableFirst.equals(null));
		
		Vegetable vegetableThird = new LeafVegetable(-CALORIES, -WEIGHT, NAME_SECOND);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new LeafVegetable(-CALORIES, -WEIGHT, null);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new LeafVegetable(CALORIES, WEIGHT, NAME_FIRST);
		Assert.assertTrue(vegetableFirst.equals(vegetableThird)); 
	}

	/**
	 * Check equals method for {@link LeguminousVegetable}
	 */
	@Test
	public void testEqualsLeguminousVegetable() {
		vegetableFirst = new LeguminousVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetableSecond = new LeguminousVegetable(CALORIES, WEIGHT, NAME_SECOND);

		Assert.assertFalse(vegetableFirst.equals(vegetableSecond));
		vegetableSecond = vegetableFirst;
		Assert.assertTrue(vegetableFirst.equals(vegetableSecond));
		Assert.assertFalse(vegetableFirst.equals(null));
		
		Vegetable vegetableThird = new LeguminousVegetable(-CALORIES, -WEIGHT, NAME_SECOND);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new LeguminousVegetable(-CALORIES, -WEIGHT, null);
		Assert.assertFalse(vegetableFirst.equals(vegetableThird)); 
		vegetableThird = new LeguminousVegetable(CALORIES, WEIGHT, NAME_FIRST);
		Assert.assertTrue(vegetableFirst.equals(vegetableThird)); 
	}

	/**
	 * Check equals for different types
	 */
	@Test
	public void testDiffVegs() {
		vegetableFirst = new LeguminousVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetableSecond = new LeafVegetable(CALORIES, WEIGHT, NAME_SECOND);
		vegetableThird = new RootVegetable(CALORIES, WEIGHT, NAME_SECOND);
		vegetableFourth = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);

		Assert.assertFalse(vegetableFirst.equals(vegetableSecond));
		Assert.assertFalse(vegetableThird.equals(vegetableFourth));

		vegetableSecond = vegetableFirst;
		Assert.assertTrue(vegetableFirst.equals(vegetableSecond));

		Assert.assertFalse(vegetableFirst.equals(null));
	}

	/**
	 * Test hashCode for different Vegetables
	 */
	@Test
	public void testHashCode() {
		vegetableFirst = new LeguminousVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetableSecond = new LeafVegetable(CALORIES, WEIGHT, NAME_SECOND);
		vegetableThird = new RootVegetable(CALORIES, WEIGHT, NAME_SECOND);
		vegetableFourth = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		
		Assert.assertNotSame(vegetableFirst.hashCode(), vegetableSecond.hashCode());
		Assert.assertNotSame(vegetableThird.hashCode(), vegetableFourth.hashCode());
	}
	
	/**
	 * Check that toString does not return something like cash with '@' 
	 * 		for different Vegetables
	 */
	@Test
	public void testToString() {
		vegetableFirst = new LeguminousVegetable(CALORIES, WEIGHT, NAME_FIRST);
		vegetableSecond = new LeafVegetable(CALORIES, WEIGHT, NAME_SECOND);
		vegetableThird = new RootVegetable(CALORIES, WEIGHT, NAME_SECOND);
		vegetableFourth = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		
		Assert.assertFalse(vegetableFirst.toString().contains("@"));
		Assert.assertFalse(vegetableSecond.toString().contains("@"));
		Assert.assertFalse(vegetableThird.toString().contains("@"));
		Assert.assertFalse(vegetableFourth.toString().contains("@"));
	}
	
	/**
	 * Check getName method
	 */
	@Test
	public void testGetName() {
		LeguminousVegetable vegetableFirst = new LeguminousVegetable(CALORIES, WEIGHT, NAME_FIRST);
		LeafVegetable vegetableSecond = new LeafVegetable(CALORIES, WEIGHT, NAME_SECOND);
		RootVegetable vegetableThird = new RootVegetable(CALORIES, WEIGHT, NAME_SECOND);
		FruitingVegetable vegetableFourth =  new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		
		Assert.assertEquals(NAME_FIRST,  vegetableFirst.getName()); 
		Assert.assertEquals(NAME_SECOND,  vegetableSecond.getName()); 
		Assert.assertEquals(NAME_SECOND,  vegetableThird.getName()); 
		Assert.assertEquals(NAME_FIRST,  vegetableFourth.getName()); 
	}
}
