package by.shevtsova.chef.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import by.shevtsova.chef.exception.LogicException;
import by.shevtsova.chef.model.vegetable.FruitingVegetable;

public class SaladTest {
	private final static int CALORIES = 15;
	private final static int WEIGHT = 10;
	private final static String NAME_FIRST = "NAME_1";
	private final static String NAME_SECOND = "NAME_2";

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	/**
	 * Check equals method for {@link Salad}
	 * 
	 * @throws LogicException
	 */
	@Test
	public void testEquals() throws LogicException {
		Vegetable vegetableFirst = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		Vegetable vegetableSecond = new FruitingVegetable(CALORIES, WEIGHT, NAME_SECOND);
		List<Vegetable> vegetables = new ArrayList<Vegetable>();
		vegetables.add(vegetableFirst);
		vegetables.add(vegetableSecond);

		Salad saladFirst = new Salad(vegetables);
		Salad saladSecond = saladFirst;
		Assert.assertTrue(saladFirst.equals(saladSecond));
		Assert.assertFalse(saladFirst.equals(null));

		saladSecond = new Salad(new ArrayList<Vegetable>());
		Assert.assertFalse(saladFirst.equals(saladSecond));
	}

	/**
	 * Check hashCode method for {@link Salad}
	 */
	@Test
	public void testHashCode() {
		Vegetable vegetableFirst = new FruitingVegetable(CALORIES, WEIGHT, NAME_FIRST);
		List<Vegetable> vegetables = new ArrayList<Vegetable>();
		vegetables.add(vegetableFirst);
		Salad saladFirst = new Salad(vegetables);
		Salad saladSecond = saladFirst;
		Assert.assertNotSame(saladFirst.hashCode(), saladSecond.hashCode());
	}

	/**
	 * Check constructor behavior
	 */
	@Test
	public void testConstructor() {
		List<Vegetable> vegetables = new ArrayList<Vegetable>();
		 vegetables.add(null);

		Salad salad = new Salad(vegetables);
		Assert.assertNotNull(salad.getVegetableList());
	}

	/**
	 * Check that toString does not return something like cash with '@'
	 */
	@Test
	public void testToString() {
		List<Vegetable> vegetables = new ArrayList<Vegetable>();

		Salad salad = new Salad(vegetables);
		
		 Assert.assertFalse(salad.toString().contains("@"));
	}
}
