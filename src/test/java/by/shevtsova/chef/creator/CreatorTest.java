package by.shevtsova.chef.creator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import by.shevtsova.chef.creator.Creator;
import by.shevtsova.chef.exception.TechnicalException;
import by.shevtsova.chef.model.Salad;
import by.shevtsova.chef.model.Vegetable;
import by.shevtsova.chef.reader.ReadFromFile;

@RunWith(MockitoJUnitRunner.class)
public class CreatorTest {
	private static final String FILE_NAME_VEGS = "info/vegetables";

	/**
	 * Check is salad created
	 * 
	 * @throws TechnicalException
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testCreate() throws TechnicalException {
		List<Vegetable> vegetableList = new ArrayList<Vegetable>();

		ReadFromFile spy = Mockito.spy(ReadFromFile.class);
		vegetableList = spy.read(FILE_NAME_VEGS, vegetableList);

		Assert.assertEquals(new Salad(vegetableList), Creator.create(FILE_NAME_VEGS));
	}

}
