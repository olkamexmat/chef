# Chef
Simple console application

# Installation

To run JDK 1.8 or above should be installed on your machine. 
Run: 'gradle build' from root directory of the project 
Execute: 'gradle execute' from root directory of the project

# Usage
Application does several things:
	1. read info from file and make Salad 
	2. calculate caloric content of salad 
	3. sort vegetables in salad by their weight 
	4. make sample from salad 
The results can see in file 'log/log.txt' after build



